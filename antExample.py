"""
class antExample
"""
import math
import random
from math import *

from pyAmakCore.classes.agent import *

class antExample(Agent):

    def __init__(self,
                 amas : 'antHillExample',
                 startX: float,
                 startY: float
                 ) -> None:
        super().__init__(amas)
        self._dead=False
        self._dx = startX
        self._dy = startY
        self._angle=random() * pi * 2

    def _on_decide_and_act(self) -> None:
        random = self.get_amas().get_environment().get_random().gauss()
        self._angle += random() * 0.1
        self._dx += math.cos(self._angle)
        self._dy += math.sin(self._angle)
        while (self._dx >= self.get_amas().get_environment()._get_width() / 2):
            self._dx -= self.get_amas().get_environment()._get_width()
        while (self._dy >= self.get_amas().get_environment()._get_height() / 2):
            self._dy -= self.get_amas().get_environment()._get_height()
        while (self._dx < -(self.get_amas().get_environment()._get_width()) / 2):
            self._dx += self.get_amas().get_environment()._get_width()
        while (self._dy < -(self.get_amas().get_environment()._get_height()) / 2):
            self._dy += self.get_amas().get_environment()._get_height()
        if self.get_amas().get_environment().get_random().random()<0.001:
            self._dead=True
            self.destroy_agent()
        if self.get_amas().get_environment().get_random().random()<0.001:
            self.__init__(self.get_amas(),self._dx,self._dy)
