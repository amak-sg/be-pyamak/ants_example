"""
class antHillExample
"""
from pyAmakCore.classes.amas import *
from antExample import *
from pyAmakCore.enumeration.scheduling import scheduling


class antHillExample(Amas):

    def __init__(self,env):
        super().__init__(env,scheduling.DEFAULT)

    def on_initial_agents_creation(self) -> None:
        for i in range(50):
            antExample(self,0,0)

    def on_system_cycle_end(self) -> None:
        #Un affichage avec l'interface
        pass