#! /bin/sh

git pull

cd ../pyamak_ihm
git pull
python3 setup.py bdist_wheel
pip3 install --force-reinstall dist/pyAmakIHM-0.0.1-py3-none-any.whl

cd ../pyamak_noyau
git pull
python3 setup.py bdist_wheel
pip3 install --force-reinstall dist/pyAmakCore-0.0.1-py3-none-any.whl

cd ../philosopher_example
python3 philosophersLaunchExample.py
