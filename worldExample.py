"""
Class worldExample
"""

from pyAmakCore.classes.environment import *
from pyAmakCore.enumeration.scheduling import scheduling


class worldExample(Environment):

    def __init__(self):
        super().__init__(scheduling.DEFAULT)
        self._width = 0 # à vérifier
        self._height = 0 # à vérifier

    def _get_width(self) -> int:
        """
        getter for width
        """
        return self._width

    def _get_height(self) -> int:
        """
        getter for height
        """
        return self._height

    def on_initialization(self) -> None:
        # à vérifier avc l'ihm
        self._width = 800
        self._height = 600